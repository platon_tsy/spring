package com.hillel.springmvc.dao;

import java.util.List;

import com.hillel.springmvc.model.UserProfile;


public interface UserProfileRepositoryDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
