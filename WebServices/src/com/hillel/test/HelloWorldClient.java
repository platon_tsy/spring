package com.hillel.test;

import com.hillel.ws.HelloWorld;

public class HelloWorldClient {
    public static void main(String[] args) {

        HelloWorldImplService helloService = new HelloWorldImplService();
        HelloWorld helloWorld = helloService.getHelloWorldImplPort();

        System.out.println(helloWorld.getHelloWorldAsString("petya"));

    }

}
