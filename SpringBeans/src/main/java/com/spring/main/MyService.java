package com.spring.main;

import org.springframework.context.annotation.Scope;

import java.util.Date;

public class MyService {

	public void log(String msg){
		System.out.println(new Date()+"::"+msg);
	}
}
