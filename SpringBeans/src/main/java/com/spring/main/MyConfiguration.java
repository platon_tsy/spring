package com.spring.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(value="com.spring")
public class MyConfiguration {

	@Bean
	@Scope(value = "prototype")
	public MyService getService(){
		return new MyService();
	}
}
