package com.hillel.spring.service;

public interface MailService {

	public void sendEmail(final Object object);
}
