package com.hillel.spring.service;

import com.hillel.spring.model.ProductOrder;

public interface OrderService {

	public void sendOrderConfirmation(ProductOrder productOrder);
	
}
