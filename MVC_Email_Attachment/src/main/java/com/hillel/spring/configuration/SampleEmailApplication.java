package com.hillel.spring.configuration;

import com.hillel.spring.model.CustomerInfo;
import com.hillel.spring.model.ProductOrder;
import com.hillel.spring.service.OrderService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SampleEmailApplication {

	public static void main(String[] args) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

		OrderService orderService = (OrderService) context.getBean("orderService");
		orderService.sendOrderConfirmation(getDummyOrder());
     	((AbstractApplicationContext) context).close();
	}
	
	public static ProductOrder getDummyOrder() {
		ProductOrder order = new ProductOrder();
		order.setOrderId("1111");
		order.setProductName("Thinkpad T510");
		order.setStatus("confirmed");
		
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setName("Hillel Admin");
		customerInfo.setAddress("WallStreet");
		customerInfo.setEmail("platon.tsy@gmail.com");
		order.setCustomerInfo(customerInfo);
		return order;
	}

}
