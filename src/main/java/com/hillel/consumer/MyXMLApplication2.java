package com.hillel.consumer;

import com.hillel.services.MessageService;

public class MyXMLApplication2 {

	private MessageService service;

	//constructor-based dependency injection
	public MyXMLApplication2(MessageService svc) {
		this.service = svc;
	}
	
	//setter-based dependency injection
	public void setService(MessageService svc){
		this.service=svc;
	}

	public boolean processMessage(String msg, String rec) {
		// some magic like validation, logging etc
		return this.service.sendMessage(msg, rec);
	}
}
