package com.hillel.services;

public interface MessageService {

	boolean sendMessage(String msg, String rec);
}
