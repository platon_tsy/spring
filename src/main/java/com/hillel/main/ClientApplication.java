package com.hillel.main;

import com.hillel.consumer.MyApplication;
import com.hillel.services.TwitterService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.hillel.configuration.DIConfiguration;
import org.springframework.context.support.GenericApplicationContext;

public class ClientApplication {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
		MyApplication app = context.getBean(MyApplication.class);
		context.getBean("myXMLApp2");
		
		app.processMessage("Hello world!", "hillel@school.com");
		
		//close the context
		context.close();
	}

}
