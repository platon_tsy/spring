package com.hillel.configuration;

import com.hillel.services.EmailService;
import com.hillel.services.MessageService;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(value="com.hillel")
//@ImportResource(value = "classpath:applicationContext.xml")
public class DIConfiguration {

	@Bean
	public MessageService getMessageService(){
		return new EmailService();
	}


}
