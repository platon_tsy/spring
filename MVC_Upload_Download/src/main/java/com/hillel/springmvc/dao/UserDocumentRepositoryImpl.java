package com.hillel.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hillel.springmvc.model.UserDocument;

@Repository("userDocumentRepository")
public class UserDocumentRepositoryImpl extends AbstractRepository<Integer, UserDocument> implements UserDocumentRepository {

	@SuppressWarnings("unchecked")
	public List<UserDocument> findAll() {
		Criteria criteria = createEntityCriteria();
		return (List<UserDocument>)criteria.list();
	}

	public void save(UserDocument document) {
		persist(document);
	}

	
	public UserDocument findById(int id) {
		return getByKey(id);
	}

	@SuppressWarnings("unchecked")
	public List<UserDocument> findAllByUserId(int userId){
		Criteria crit = createEntityCriteria();
		Criteria userCriteria = crit.createCriteria("user");
		userCriteria.add(Restrictions.eq("id", userId));
		return (List<UserDocument>)crit.list();
	}

	
	public void deleteById(int id) {
		UserDocument document =  getByKey(id);
		delete(document);
	}

}
