package com.hillel.springmvc.dao;

import java.util.List;

import com.hillel.springmvc.model.UserDocument;

public interface UserDocumentRepository {

	List<UserDocument> findAll();
	
	UserDocument findById(int id);
	
	void save(UserDocument document);
	
	List<UserDocument> findAllByUserId(int userId);
	
	void deleteById(int id);
}
